-module(game).
-behavior(gen_statem).
-export([start_link/1, init/1, callback_mode/0, handle_event/4]).

% Client API
-export([play_game/1, leave_game/1, roll_dice/1, select_piece/2]).

% To be used from game_graphics
-export([broadcast/2]).

callback_mode() -> handle_event_function.

start_link(Args) ->
    gen_statem:start_link(?MODULE, Args, []).

init(Client) ->
    GameData = #{players => [Client],
		 last_roll => undefined,
		 current_player => Client,
		 board => undefined},
    {ok, waiting_for_players, GameData}.

% joining the game
handle_event({call, From}, {join, NewClient=#{name := Name}}, waiting_for_players,
	     Data=#{players := Players}) ->
    GameNotFull = 4 > length(Players),
    if
	GameNotFull ->
	    broadcast("~s joined the game\n", [Name]),
	    NewData = Data#{players := [NewClient|Players]},
	    {next_state, waiting_for_players, NewData, {reply, From, {ok, self()}}};
	GameNotFull =:= false ->
	    {next_state, waiting_for_players, Data, {reply, From, {game_full, self()}}}
    end;

% joining too late
handle_event({call, From}, {join, _NewClient}, State, Data) ->
    {next_state, State, Data, {reply, From, {game_has_begun, self()}}};

% starting the game
handle_event(cast, play, waiting_for_players, Data=#{current_player := Player, players := Players}) ->
    Board = game_logic:new_board(Players),
    game_graphics:draw_board(Players, Board),
    lists:zipwith(fun(N, P) ->
		      send(P, "You have the color ~p\n", [game_graphics:number_to_color(N)])
		  end, lists:seq(1, length(Players)), Players),
    send(Player, "It is your turn to roll the dice\n"),
    {next_state, dice_roll, Data#{board := Board}};

% leaving the game
handle_event(cast, {leave, ClientID}, State, Data=#{current_player := Current,
						    players := Players,
						    board := Board}) ->
    [Client=#{name := Name}] = lists:filter(fun(C) ->
						    is_correct_client(ClientID, C)
					    end, Players),
    NewPlayers = Players -- [Client],
    NewBoard = maps:remove(ClientID, Board),
    broadcast("~s left the game\n", [Name]),
    NewCurrent = if
		     Client =:= Current ->
			 game_logic:next_player(Current, Players);
		     Client =/= Current ->
			 Current
		 end,
    if
	NewPlayers =:= [] ->
	    supervisor:terminate_child(game_manager, self());
	NewPlayers =/= [] ->
	    {next_state, State, Data#{players := NewPlayers, current_player := NewCurrent, board := NewBoard}}
    end;

% roll the dice
handle_event(cast, {roll_dice, ClientID}, dice_roll, Data=#{current_player := Player,
							    board := Board,
							    players := Players}) ->
    AllowedToRoll = is_correct_client(ClientID, Player),
    case AllowedToRoll of
	true ->
	    Roll = game_logic:roll_dice(),
	    PlayerName = maps:get(name, Player),
	    broadcast("~s rolled a ~p\n", [PlayerName, Roll]),
	    AllowedPieces = game_logic:player_pieces(Player, Board, Roll),
	    if
		AllowedPieces =:= [] ->
		    send(ClientID, "You can't move anything with that\n"),
		    NextPlayer = game_logic:next_player(Player, Players),
		    send(NextPlayer, "It is your turn to roll the dice\n"),
		    {next_state, dice_roll, Data#{current_player := NextPlayer}};
		AllowedPieces =/= [] ->
		    send(ClientID, "Select a piece you want to move ~p\n", [AllowedPieces]),
		    {next_state, select_piece_to_move, Data#{last_roll := Roll}}
	    end;
	false ->
	    send(ClientID, "It is not your turn to roll the dice.\n"),
	    keep_state_and_data
    end;

% select which piece to move
handle_event(cast, {move_piece, Number, ClientID}, select_piece_to_move,
	     Data=#{current_player := Player=#{id := ClientID}, players := Players}) ->
    case game_logic:move_piece(Number, Data) of
	{ok, {NewBoard, NewPlayer}} ->
	    game_graphics:draw_board(Players, NewBoard),
	    send(NewPlayer, "It is your turn to roll the dice\n"),
	    {next_state, dice_roll, Data#{current_player := NewPlayer, board := NewBoard}};
	wrong_piece ->
	    send(Player, "You can't move that piece\n"),
	    keep_state_and_data;
	{game_done, {NewBoard, Winner}} ->
	    game_graphics:draw_board(Players, NewBoard),
	    WinnerName = maps:get(name, Winner),
	    broadcast("~s wins the game!\n", [WinnerName]),
	    lists:foreach(fun(#{id := Client}) ->
				  client:game_done(Client)
			  end, Players),
	    keep_state_and_data
    end;

% send a message to a single player
handle_event(cast, {send_msg, Msg, ClientID}, _State, _Data) ->
    client:send(ClientID, Msg),
    keep_state_and_data;

% broadcasting a message to all players
handle_event(cast, {broadcast_msg, Msg}, _State, #{players := Players}) ->
    lists:foreach(fun(#{id := ClientID}) ->
			  client:send(ClientID, Msg)
		  end, Players),
    keep_state_and_data;

% unknown events
handle_event(Type, Content, State, Data) ->
    io:format("game: unknown event happened (~p) ~p while in state ~p~n",
	      [Type, Content, State]),
    {next_state, State, Data}.

%% client API
play_game(GameID) ->
    gen_statem:cast(GameID, play).

leave_game(GameID) ->
    gen_statem:cast(GameID, {leave, self()}).

roll_dice(GameID) ->
    gen_statem:cast(GameID, {roll_dice, self()}).

select_piece(GameID, Number) ->
    gen_statem:cast(GameID, {move_piece, Number, self()}).

%% Private functions

% does the client and the client id match?
is_correct_client(ClientID, #{id := ClientID}) ->
    true;
is_correct_client(_, _) ->
    false.

% send a message to all clients in the game
broadcast(FormatString, Args) ->
    Message = lists:flatten(io_lib:format(FormatString, Args)),
    broadcast(Message).
broadcast(Message) ->
    gen_statem:cast(self(), {broadcast_msg, Message}).

% send a message to a singe client
send(Client, FormatString, Args) ->
    Message = lists:flatten(io_lib:format(FormatString, Args)),
    send(Client, Message).
send(#{id := ClientID}, Message) ->
    send(ClientID, Message);
send(ClientID, Message) ->
    gen_statem:cast(self(), {send_msg, Message, ClientID}).
