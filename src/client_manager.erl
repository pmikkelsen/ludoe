-module(client_manager).
-behavior(supervisor).
-export([start_link/0, init/1, acceptor/1]).

start_link() ->
    supervisor:start_link({local, client_manager}, client_manager, []).

init(_Args) ->
    SupFlags = #{strategy => simple_one_for_one,
		 intensity => 5,
		 period => 10},
    ChildSpec = #{id => 'client',
		  start => {client, start_link, []},
		  restart => temporary,
		  shutdown => brutal_kill,
		  type => worker,
		  modules => [client]},
    {ok, {SupFlags, [ChildSpec]}}.

acceptor(ListenSocket) ->
    {ok, Socket} = gen_tcp:accept(ListenSocket),
    {ok, Pid} = supervisor:start_child(client_manager, [Socket]),
    gen_tcp:controlling_process(Socket, Pid).
