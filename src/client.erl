-module(client).
-behavior(gen_statem).
-export([start_link/1, init/1, callback_mode/0, handle_event/4, send/2, game_done/1]).

-define(BinIn(Sock, Str), {tcp, Sock, <<Str, _/binary>>}).

callback_mode() -> handle_event_function.

start_link(Socket) ->
    gen_statem:start_link(?MODULE, Socket, []).

init(Socket) ->
    send("Hello, what is you name? "),
    Client = #{sock => Socket,
	       game => undefined,
	       name => "unnamed player"},
    {ok, get_name, Client}.

% get user name
handle_event(info, {tcp, Sock, Name}, get_name, Data=#{sock := Sock}) ->
    RealName = drop_whitespace(Name),
    send("Do you want to host or join a game? (host/join) "),
    {next_state, host_or_join, Data#{name := RealName}};

% select to host game
handle_event(info, ?BinIn(Sock, "host"), host_or_join, Data=#{sock := Sock,
							      name := Name}) ->
    {ok, GameID} = game_manager:host_game(Name),
    send("Your game is running with id " ++ pid_to_list(GameID) ++ "\n"),
    send("Press enter to start the game\n"),
    {next_state, wait_for_start, Data#{game := GameID}};

% select to join game
handle_event(info, ?BinIn(Sock, "join"), host_or_join, Data=#{sock := Sock}) ->
    send("What game id do you want to join? "),
    {next_state, join, Data};

% input of game id for join
handle_event(info, {tcp, Sock, JoinId}, join, Data=#{sock := Sock,
						     name := Name}) ->
    case parse_gameid(JoinId) of
	parse_error ->
	    send("That is not a valid game id. The format is <n.n.n>\n"),
	    keep_state_and_data;
	GameID ->
	    game_manager:join_game(Name, GameID),
	    {next_state, joining, Data#{game := GameID}}
    end;

% failed join. the game was on
handle_event(cast, {game_has_begun, GameID}, joining, Data=#{game := GameID}) ->
    send("The game has already begun. Host or join a new game? (host/join) "),
    {next_state, host_or_join, Data#{game := undefined}};

% failed join. game was full
handle_event(cast, {game_full, GameID}, joining, Data=#{game := GameID}) ->
    send("The game is full. Host or join a new game? (host/join) "),
    {next_state, host_or_join, Data#{game := undefined}};

% failed join. game does not exist (or did not respond)
handle_event(cast, {game_no_exist, GameID}, joining, Data=#{game := GameID}) ->
    send("The game did not exist or did not respond. Host or join a new game? (host/join) "),
    {next_state, host_or_join, Data#{game := undefined}};

% successful join
handle_event(cast, {ok, GameID}, joining, Data=#{game := GameID}) ->
    {next_state, in_game, Data};

% start game
handle_event(info, ?BinIn(Sock, ""), wait_for_start, Data=#{sock := Sock, game := GameID}) ->
    game:play_game(GameID),
    {next_state, in_game, Data};

% quit game
handle_event(info, ?BinIn(Sock, "quit"), _State, #{sock := Sock, game := GameID}) ->
    game:leave_game(GameID),
    {stop, normal};

% connection dropped
handle_event(info, {tcp_closed, _}, _State, #{game := GameID}) ->
    game:leave_game(GameID),
    {stop, normal};

% game done
handle_event(cast, {game_done, GameID}, in_game, Data=#{game := GameID}) ->
    send("Host or join a new game? (host/join) "),
    game:leave_game(GameID),
    {next_state, host_or_join, Data};

% roll dice
handle_event(info, ?BinIn(Sock, "roll"), in_game, #{game := GameID,
						    sock := Sock}) ->
    game:roll_dice(GameID),
    keep_state_and_data;

% select piece
handle_event(info, {tcp, Sock, Msg}, in_game, #{game := GameID,
						sock := Sock}) ->
    StrMsg = drop_whitespace(Msg),
    case string:to_integer(StrMsg) of
	{error, _} ->
	    send("command \"" ++ StrMsg ++ "\" not understood.\n");
	{Int, ""} ->
	    game:select_piece(GameID, Int)
    end,
    keep_state_and_data;

% unknown tcp input
handle_event(info, {tcp, Sock, Msg}, _State, #{sock := Sock}) ->
    RealMsg = drop_whitespace(Msg),
    send("command \"" ++ RealMsg ++ "\" not understood.\n"),
    keep_state_and_data;

% send a tcp message to client
handle_event(cast, {tcp_send, Msg}, _State, #{sock := Sock}) ->
    gen_tcp:send(Sock, Msg),
    keep_state_and_data;

% unknown events
handle_event(EventType, Content, State, Data) ->
    io:format("client: unknown event happened (~p) ~p while in state ~p~n",
	      [EventType, Content, State]),
    {next_state, State, Data}.


%% to be called from game
game_done(ClientID) ->
    gen_statem:cast(ClientID, {game_done, self()}).

%%% private functions

% parse a game id
parse_gameid(String) ->
    RealString = drop_whitespace(String),
    try list_to_pid(RealString)
    catch
	_:_ -> parse_error
    end.

% drop whitespace. assumes all input ends in \r\n
drop_whitespace(BinString) when is_binary(BinString) ->
    drop_whitespace(binary_to_list(BinString));
drop_whitespace(String) ->
    try lists:droplast(lists:droplast(String))
    catch
	_:_ -> ""
    end.

% send a tcp message to the client
send(Message) ->
    send(self(), Message).
send(Client, Message) ->
    gen_statem:cast(Client, {tcp_send, Message}).
