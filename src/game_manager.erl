-module(game_manager).
-behavior(supervisor).
-export([start_link/0, init/1, host_game/1, join_game/2]).

start_link() ->
    supervisor:start_link({local, game_manager}, game_manager, []).

init(_Args) ->
    SupFlags = #{strategy => simple_one_for_one,
		 intensity => 5,
		 period => 10},
    ChildSpec = #{id => 'game',
		  start => {game, start_link, []},
		  restart => temporary,
		  modules => [game]},
    {ok, {SupFlags, [ChildSpec]}}.

host_game(ClientName) ->
    Client = #{id => self(),
	       name => ClientName},
    supervisor:start_child(game_manager, [Client]).

join_game(ClientName, GameID) ->
    Client = #{id => self(),
	       name => ClientName},
    try gen_statem:call(GameID, {join, Client}, 1000) of
	Reply -> gen_statem:cast(self(), Reply)
    catch
	_:_ ->
	    gen_statem:cast(self(), {game_no_exist, GameID})
    end.
