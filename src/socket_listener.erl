-module(socket_listener).
-export([start_link/0]).

start_link() ->
    Pid = spawn_link(fun listen/0),
    {ok, Pid}.

listen() ->
    {ok, Port} = application:get_env(port),
    {ok, Listen} = gen_tcp:listen(Port, [binary, {active, true}, {reuseaddr, true}]),
    acceptor(Listen).

acceptor(ListenSocket) ->
    client_manager:acceptor(ListenSocket),
    acceptor(ListenSocket).
