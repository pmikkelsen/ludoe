-module(ludoserver_sup).
-behavior(supervisor).
-export([start_link/0, init/1]).

start_link() ->
    supervisor:start_link(ludoserver_sup, []).

init(_Args) ->
    SupFlags = #{strategy => one_for_all,
		 intensity => 5,
		 period => 10},
    ChildSpecSockListener = #{id => 'sock_listener',
			      start => {socket_listener, start_link, []},
			      modules => [socket_listener]},
    ChildSpecClientManager = #{id => 'client_manager',
			       start => {client_manager, start_link, []},
			       type => supervisor,
			       modules => [client_manager]},
    ChildSpecGameManager = #{id => 'game_manager',
			     start => {game_manager, start_link, []},
			     type => supervisor,
			     modules => [game_manager]},
    ChildSpecs = [ChildSpecSockListener,
		  ChildSpecClientManager,
		  ChildSpecGameManager],
    {ok, {SupFlags, ChildSpecs}}.
