-module(game_graphics).
-export([number_to_color/1, draw_board/2]).

color(Num, String) when is_number(Num) ->
    color(number_to_color(Num), String);
color(red, String) ->
    "\x1b[31m" ++ String ++ "\x1b[0m";
color(green, String) ->
    "\x1b[32m" ++ String ++ "\x1b[0m";
color(yellow, String) ->
    "\x1b[33m" ++ String ++ "\x1b[0m";
color(blue, String) ->
    "\x1b[34m" ++ String ++ "\x1b[0m".

number_to_color(1) ->
    red;
number_to_color(2) ->
    green;
number_to_color(3) ->
    yellow;
number_to_color(4) ->
    blue.

draw_board(Players, Board) ->
    PositionChars = get_position_chars(Players, Board),
    game:broadcast(
"                        *---*---*---*\n"
"                        |~s|~s|~s|\n"
"                        *---*---*---*   *---*---*\n"
"                        |~s|~s|~s|   | ~s | ~s |\n"
"                        *---*---*---*   *---*---*\n"
"                        |~s|~s|~s|   | ~s | ~s |\n"
"    *---*---*           *---*---*---*   *---*---*\n"
"    | ~s | ~s |           |~s|~s|~s|\n"
"    *---*---*           *---*---*---*\n"
"    | ~s | ~s |           |~s|~s|~s|\n"
"    *---*---*           *---*---*---*\n"
"                        |~s|~s|~s|\n"
"*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*\n"
"|~s|~s|~s|~s|~s|~s|##/~s~s ~s~s\\##|~s|~s|~s|~s|~s|~s|\n"
"*---*---*---*---*---*---*/         \\*---*---*---*---*---*---*\n"
"|~s|~s|~s|~s|~s|~s|~s~s~s~s   ~s~s~s~s|~s|~s|~s|~s|~s|~s|\n"
"*---*---*---*---*---*---*\\         /*---*---*---*---*---*---*\n"
"|~s|~s|~s|~s|~s|~s|##\\~s~s ~s~s/##|~s|~s|~s|~s|~s|~s|\n"
"*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*\n"
"                        |~s|~s|~s|                  \n"
"                        *---*---*---*           *---*---* \n"
"                        |~s|~s|~s|           | ~s | ~s |\n"
"                        *---*---*---*           *---*---*\n"
"                        |~s|~s|~s|           | ~s | ~s |\n"
"            *---*---*   *---*---*---*           *---*---*\n"
"            | ~s | ~s |   |~s|~s|~s|\n"
"            *---*---*   *---*---*---*\n"
"            | ~s | ~s |   |~s|~s|~s|\n"
"            *---*---*   *---*---*---*\n"
"                        |~s|~s|~s|\n"
"                        *---*---*---*\n",
	      PositionChars).

get_position_chars(Players, Board) ->
    Mapping =
	[12, 13, 14,
	 11, {end_run, 2, 1}, 15, {home, 2, 1}, {home, 2, 2},
	 10, {end_run, 2, 2}, 16, {home, 2, 3}, {home, 2, 4},
	 {home, 1, 1}, {home, 1, 2}, 9, {end_run, 2, 3}, 17,
	 {home, 1, 3}, {home, 1, 4}, 8, {end_run, 2, 4}, 18,
	 7, {end_run, 2, 5}, 19,
	 1, 2, 3, 4, 5, 6, {goal, 2, 1}, {goal, 2, 2}, {goal, 2, 3}, {goal, 2, 4}, 20, 21, 22, 23, 24, 25,
	 0, {end_run, 1, 1}, {end_run, 1, 2}, {end_run, 1, 3}, {end_run, 1, 4}, {end_run, 1, 5},
	 {goal, 1, 4}, {goal, 1, 3},  {goal, 1, 2},  {goal, 1, 1},  {goal, 3, 1},  {goal, 3, 2},  {goal, 3, 3},  {goal, 3, 4},
	 {end_run, 3, 5}, {end_run, 3, 4}, {end_run, 3, 3}, {end_run, 3, 2}, {end_run, 3, 1}, 26,
	 51, 50, 49, 48, 47, 46, {goal, 4, 1}, {goal, 4, 2}, {goal, 4, 3}, {goal, 4, 4}, 32, 31, 30, 29, 28, 27,
	 45, {end_run, 4, 5}, 33,
	 44, {end_run, 4, 4}, 34, {home, 3, 1}, {home, 3, 2},
	 43, {end_run, 4, 3}, 35, {home, 3, 3}, {home, 3, 4},
	 {home, 4, 1}, {home, 4, 2}, 42, {end_run, 4, 2}, 36,
	 {home, 4, 3}, {home, 4, 4}, 41, {end_run, 4, 1}, 37,
	 40, 39, 38
	],

    lists:map(fun(Pos) ->
		      get_position_char(Pos, Players, Board)
	      end, Mapping).

get_position_char({home, Player, Number}, Players, Board) ->
    Numbers = has_piece(Player, Players, home, Board),
    case lists:member(Number, Numbers) of
	true -> color(Player, integer_to_list(Number));
	false -> " "
    end;
get_position_char({goal, Player, Number}, Players, Board) ->
    Numbers = has_piece(Player, Players, goal, Board),
    case lists:member(Number, Numbers) of
	true -> color(Player, integer_to_list(Number));
	false -> " "
    end;
get_position_char(Pos, Players, Board) ->
    case who_has_piece(Pos, Players, Board) of
	no_one -> "   ";
	[{PlayerNumber, PieceNumber}] ->
	    PieceStr = integer_to_list(PieceNumber),
	    " " ++ color(PlayerNumber, PieceStr) ++ " ";
	[{PlayerNumber1, PieceNumber1}, {PlayerNumber2, PieceNumber2}] ->
	    PieceStr1 = integer_to_list(PieceNumber1),
	    PieceStr2 = integer_to_list(PieceNumber2),
	    color(PlayerNumber1, PieceStr1) ++ " " ++ color(PlayerNumber2, PieceStr2);
	[{PlayerNumber1, PieceNumber1}, {PlayerNumber2, PieceNumber2}, {PlayerNumber3, PieceNumber3}|_] ->
	    PieceStr1 = integer_to_list(PieceNumber1),
	    PieceStr2 = integer_to_list(PieceNumber2),
	    PieceStr3 = integer_to_list(PieceNumber3),
	    color(PlayerNumber1, PieceStr1) ++ color(PlayerNumber2, PieceStr2) ++ color(PlayerNumber3, PieceStr3)
    end.

who_has_piece(Pos, Players, Board) ->
    Res = lists:map(fun(N) ->
			    {N, has_piece(N, Players, Pos, Board)}
		    end, [1,2,3,4]),
    HasPiece = lists:filter(fun({_N, []}) ->
				    false;
			       ({_N, _PieceNumbers}) ->
				    true
			    end, Res),
    case HasPiece of
	[] ->
	    no_one;
	Results ->
	    FlatFun = fun({PlayerNumber, PieceNumbers}) ->
			      lists:map(fun(PieceNumber) ->
						{PlayerNumber, PieceNumber}
					end, PieceNumbers)
		      end,
	    lists:flatten(lists:map(FlatFun, Results))
    end.

has_piece(PlayerNumber, Players, Pos, Board) ->
    PlayerID = try lists:nth(PlayerNumber, Players) of
		   #{id := ID} ->
		       ID
	       catch
		   _:_ ->
		       undefined
	       end,
    Pieces = maps:get(PlayerID, Board, maps:new()),
    PiecesOnPos = maps:to_list(maps:filter(fun(_K,V) ->
						   V =:= Pos
					   end, Pieces)),
    lists:map(fun({PieceNumber,_}) -> PieceNumber end, PiecesOnPos).
