-module(game_logic).
-export([roll_dice/0, move_piece/2, new_board/1, player_pieces/3, next_player/2]).

%% The board is a map indexed by clientIDs
%% #{id1 => #{1 => pos1,
%%            2 => goal,
%%            3 => home,
%%            4 => {end_run, 1, pos}}}
%% The numbering of the map goes like this
%%                  12 13 14 end2
%%                  11    15
%%                  10    16
%%                  09    17
%%                  08    18
%% end1             07    19             end3
%% 01 02 03 04 05 06        20 21 22 23 24 25
%% 00 01 02 03 04 05  goal                 26
%% 51 50 49 48 47 46        32 31 30 29 28 27
%%                  45    33
%%                  44    34
%%                  43    35
%%                  42    36
%%                  41    37
%%                  40 39 38 end4

roll_dice() ->
    rand:uniform(6).

new_board(Players) ->
    BoardList = lists:map(fun(#{id := ClientID}) ->
				  {ClientID, #{1 => home,
					       2 => home,
					       3 => home,
					       4 => home}}
			  end, Players),
    maps:from_list(BoardList).

move_piece(PieceNumber, #{board := Board,
			  last_roll := Roll,
			  current_player := Player,
			  players := Players}) ->
    AllowedPieces = player_pieces(Player, Board, Roll),
    case lists:member(PieceNumber, AllowedPieces) of
	false ->
	    wrong_piece;
	true ->
	    ClientID = maps:get(id, Player),
	    PlayerPieces = maps:get(ClientID, Board),
	    CurrentPos = maps:get(PieceNumber, PlayerPieces),
	    PlayerNumber = player_number(Player, Players),
	    NewPos = next_pos(CurrentPos, PlayerNumber, Roll),
	    NewBoard = send_home(Board, NewPos, ClientID),
	    NewPlayerPieces = PlayerPieces#{PieceNumber := NewPos},
	    FinalBoard = NewBoard#{ClientID := NewPlayerPieces},
	    NextPlayer = if
			     Roll =:= 6 ->
				 Player;
			     Roll =/= 6 ->
				 next_player(Player, Players)
			 end,
	    GameHasWinner = game_done(NewPlayerPieces),
	    if
		GameHasWinner ->
		    {game_done, {FinalBoard, Player}};
		GameHasWinner =:= false ->
		    {ok, {FinalBoard, NextPlayer}}
	    end
    end.

% update the board so no players are at Pos
send_home(Board, Pos, ClientId) ->
    Fun = fun(Client, Pieces, B) ->
		  if
		      Client =:= ClientId ->
			  B;
		      Client =/= ClientId ->
			  NewPieces = send_home(Pieces, Pos),
			  maps:update(ClientId, NewPieces, B)
		  end
	  end,
    maps:fold(Fun, Board, Board).

send_home(Pieces, Pos) ->
    Fun = fun(Piece, P, Positions) ->
		  if
		      P =:= Pos ->
			  maps:update(Piece, home, Positions);
		      P =/= Pos ->
			  Positions
		  end
	  end,
    maps:fold(Fun, Pieces, Pieces).

% the peaces that are not done yet
player_pieces(#{id := ClientID}, Board, Roll) ->
    Pieces = maps:get(ClientID, Board),
    NotDone = fun(_Piece, goal) ->
		      false;
		 (_Piece, _Pos) ->
		      true
	      end,
    NotHome = fun(_Piece, home) ->
		      false;
		 (_Piece, _Pos) ->
		      true
	      end,
    NotDonePieces = maps:filter(NotDone, Pieces),
    AllowedPieces = if
			Roll =:= 6 ->
			    NotDonePieces;
			Roll =/= 6 ->
			    maps:filter(NotHome, NotDonePieces)
		    end,
    maps:keys(AllowedPieces).

next_pos(CurrentPos, PlayerNumber, Roll) ->
    MoveType = move_type(CurrentPos, PlayerNumber, Roll),
    case MoveType of
	normal ->
	    (CurrentPos + Roll) rem 52;
	out ->
	    2 + 13 * (PlayerNumber - 1);
	to_end ->
	    ToTurningPoint = case PlayerNumber of
				 1 -> if
					  CurrentPos =:= 0 ->
					      0;
					  CurrentPos =/= 0 ->
					      52 - CurrentPos
				      end;
				 2 -> 13 - CurrentPos;
				 3 -> 26 - CurrentPos;
				 4 -> 39 - CurrentPos
			     end,
	    if
		Roll - ToTurningPoint =:= 6 ->
		    goal;
		Roll - ToTurningPoint =/= 6 ->
		    {end_run, PlayerNumber, Roll - ToTurningPoint}
	    end;
	end_run ->
	    {end_run, PlayerNumber, EndRunPos} = CurrentPos,
	    if
		EndRunPos + Roll < 6 ->
		    {end_run, PlayerNumber, EndRunPos + Roll};
		EndRunPos + Roll > 6 ->
		    ToEnd = 6 - EndRunPos,
		    {end_run, PlayerNumber, 6 - (Roll - ToEnd)}
	    end;
	goal ->
	    goal
    end.

move_type(home, _PlayerNumber, 6) ->
    out;
move_type({end_run, PlayerNumber, Pos}, PlayerNumber, Roll) when Pos + Roll =:= 6 ->
    goal;
move_type({end_run, PlayerNumber, _Pos}, PlayerNumber, _Roll) ->
    end_run;
move_type(Pos, 1, Roll) when Pos + Roll > 52;
			     Pos =:= 0 ->
    to_end;
move_type(Pos, PlayerNumber, Roll) when Pos + Roll > (PlayerNumber - 1) * 13,
					Pos =< (PlayerNumber - 1) * 13 ->
    to_end;
move_type(_, _, _) ->
    normal.


game_done(#{1 := goal, 2 := goal, 3 := goal, 4 := goal}) ->
    true;
game_done(_) ->
    false.

player_number(Player, [Player|_]) ->
    1;
player_number(Player, [_,Player|_]) ->
    2;
player_number(Player, [_,_,Player|_]) ->
    3;
player_number(Player, [_,_,_,Player]) ->
    4.

next_player(CurrentPlayer, [CurrentPlayer]) ->
    CurrentPlayer;
next_player(CurrentPlayer, [CurrentPlayer, Next|_]) ->
    Next;
next_player(CurrentPlayer, [_,CurrentPlayer, Next|_]) ->
    Next;
next_player(CurrentPlayer, [_,_,CurrentPlayer, Next|_]) ->
    Next;
next_player(_CurrentPlayer, [Next|_]) ->
    Next.
